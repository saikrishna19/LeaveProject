package com.controller;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.DAO.LeaveDAOImpl;
import com.LeaveHelper.LeaveHelper;
import com.model.Leave;

@Configuration
@Controller
public class LeaveController {
	@Autowired
	LeaveDAOImpl leaveDAOImpl;
	@RequestMapping("/leaveform")
	public ModelAndView showinput() {
		System.out.println("hi");
		return new ModelAndView("leaveform");
	}
	@RequestMapping("/index")
	public ModelAndView home() {
		return new ModelAndView("index.jsp");
	}
	@RequestMapping(value = "/insert")
	public ModelAndView save(@RequestParam("name") String name,@RequestParam("stdate") String stdate, @RequestParam("eddate")String eddate,@RequestParam("message")String message) {
		System.out.println("controller");
		ModelAndView mv=new ModelAndView();
		 SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-DD");
		 try {
			Date sdate =  (Date) formatter.parse(stdate);
			Date edate= (Date) formatter.parse(eddate);
			System.out.println("sdate");
			if(LeaveHelper.insert(name, sdate, edate, message))
				mv.setViewName("success");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return mv;
	}
	@RequestMapping("/retrive")
	public ModelAndView viewdata() {
		List<Leave> list = leaveDAOImpl.getAllLeaves();
		return new ModelAndView("viewleaves", "list", list);

	}
}
