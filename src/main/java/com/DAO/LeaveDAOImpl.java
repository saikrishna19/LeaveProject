package com.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.model.Leave;

public class LeaveDAOImpl {
	static JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public static int save(Leave leave) {
		System.out.println("DAO");
		String sql = "insert into project values('" + leave.getName() + "','" + leave.getStdate() + "','"
				+ leave.getEddate() + "','" + leave.getMessage() + "')";
		return jdbcTemplate.update(sql);
	}

	public List<Leave> getAllLeaves() {
		return jdbcTemplate.query("select * from project", new RowMapper<Leave>() {

			public Leave mapRow(ResultSet rs, int r) throws SQLException {
				Leave leave = new Leave();
				leave.setName(rs.getString(1));
				leave.setStdate(rs.getDate(2));
				leave.setEddate(rs.getDate(3));
				leave.setMessage(rs.getString(4));
				return leave;

			}

		});
	}

}
