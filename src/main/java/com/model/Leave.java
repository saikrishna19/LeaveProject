package com.model;

import java.util.Date;

public class Leave {
	private String name;
	private Date stdate;
	private Date eddate;
	private String message;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStdate() {
		return stdate;
	}

	public void setStdate(Date stdate) {
		this.stdate = stdate;
	}

	public Date getEddate() {
		return eddate;
	}

	public void setEddate(Date eddate) {
		this.eddate = eddate;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Leave [name=" + this.name + ", stdate=" + this.stdate + ", eddate=" + this.eddate + ", message="
				+ this.message + "]";
	}

}
