<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html"; > 
  <meta name="viewport" content="width=device-width, initial-scale=1" charset=UTF-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
body  {
    background-image: url("/home/saikrishna/Downloads/predera.jpg");
    background-color: #e6ebe0;
}
</style>
</head>
<body>
<div class="jumbotron">
<h1 align='center'><div class="page-header"><font color="orange">Leaves Data</font></div></h1>  
<table class="table table-striped">  
<thead>
<tr><th>Name</th><th>StartDate</th><th>EndDate</th><th>Reason</th></tr>  </thead>
   <c:forEach var="lev" items="${list}">   
   <tbody>
   <tr>  
   <td>${lev.name}</td>  
   <td>${lev.stdate}</td>  
   <td>${lev.eddate}</td>  
   <td>${lev.message}</td>    
   </tr>  
    </tbody>
   </c:forEach>  
   </table>  
   <br/>  
 <h2 align='center'> <button type="button" class="btn btn-primary active"><a href="index.jsp" > <font color="white"> Home</font></a></button>  
  <button type="button" class="btn btn-primary active">   <a href="leaveform"> <font color="white">LeaveForm </font></a></button></h2> 
 </div>
</body>
</html>