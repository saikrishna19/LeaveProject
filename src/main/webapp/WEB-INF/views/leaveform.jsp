<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html"; >
<meta name="viewport" content="width=device-width, initial-scale=1"
	charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
body  {
    background-image: url("/home/saikrishna/Downloads/predera.jpg");
    background-color: #b3f0ff;
}
</style>
<title>Insert title here</title>
</head>
<body>
	<div class="controller">
		<h2>
			<h2 align="center">
				<button type="button" class="btn btn-primary active">
					<a href="index.jsp"> <font color="white">Home</font></a>
				</button>
			</h2>
			<form action="insert" modelAttribute="leave">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					 <input id="email" type="text" class="form-control" name="name"	placeholder="Full Name" required>
				</div>
				<br>
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> 
					<input id="password" type="text" class="form-control" name="stdate"	placeholder="Leave Start Date(YYYY-MM-DD)" required>
				</div>
				<br>
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
					 <input id="password" type="text" class="form-control" name="eddate" placeholder="Leave End Date(YYYY-MM-DD)" required>
				</div>
				<br>
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-comment"></i></span>
					 <input id="password" type="text" class="form-control" name="message" placeholder="Message" required>
				</div>
				<br>

				<h2 align="center"><button align="center" type="submit" class="btn btn-default">Submit</button></h2>
			</form>
		</h2>
	</div>
</body>
</html>